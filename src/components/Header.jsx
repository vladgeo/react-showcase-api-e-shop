import React from 'react'

function Header() {
    return <nav className='deep-purple darken-3'>
        <div className='nav-wrapper'>
            <a href='https://vlad-ursu.com/' className='brand-logo'>
                <img src='https://vlad-ursu.com/triangle/img/logoCanvasW.svg' alt='Logo triangle'/>
            </a>
            <ul id='nav-mobile' className='right hide-on-med-and-down'>
                <li>
                    <a href='https://bitbucket.org/vladgeo/react-showcase-api-e-shop/src/master/'>Repo</a>
                </li>
            </ul>
        </div>
    </nav>
}

export {Header}