import React from 'react'
import {calcToPrice} from "../services/utils";

function BasketItem(props) {
    const {
        id,
        name,
        price,
        icon,
        quantity,
        removeFromBasket = Function.prototype,
        incQuantity = Function.prototype,
        decQuantity = Function.prototype,
    } = props;

    return <li className='collection-item avatar'>
        <img className='circle image-icon' src={icon} alt={name}/>
        <span className="title">{name}</span>
        <br/>
        <button className='btn basket-btn'>
            <i
                className='material-icons basket-quantity-item'
                onClick={() => decQuantity(id)}
            >
                remove
            </i>
        </button>
        x{quantity}
        <button className='btn basket-btn'>
            <i
                className='material-icons basket-quantity-item'
                onClick={() => incQuantity(id)}
            >
                add
            </i>
        </button>
        = ${calcToPrice(price, quantity)}
        <span
            className='secondary-content'
            onClick={() => removeFromBasket(id)}
        >
            <i className='material-icons basket-delete-item'>close</i>
        </span>
    </li>
}

export {BasketItem}