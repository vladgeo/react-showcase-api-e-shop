import React from 'react'
import {BasketItem} from "./BasketItem";
import {calcToPrice} from "../services/utils";

function BasketList(props) {
    const {order = [],
        handleBasketShow = Function.prototype,
        removeFromBasket = Function.prototype,
        incQuantity,
        decQuantity,
    } = props;

    const totalPrice = order.reduce((sum, el) => {
        return sum + el.price * el.quantity
    }, 0);

    return (
        <ul className='collection basket-list'>
            <li className='collection-item active'>Cart</li>
            {
                order.length ? order.map(item => (
                    <BasketItem
                        key={item.id}
                        removeFromBasket={removeFromBasket}
                        incQuantity={incQuantity}
                        decQuantity={decQuantity}
                        {...item}
                    />
                )) : (
                    <li className='collection-item'>Cart is empty</li>
                )
            }
            <li className='collection-item active'>Total cost: ${calcToPrice(totalPrice)}</li>
            <i className='material-icons basket-close' onClick={handleBasketShow}>close</i>
        </ul>
    )
}

export {BasketList}