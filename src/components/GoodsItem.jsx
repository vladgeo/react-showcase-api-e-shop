import React from 'react'
import {calcToPrice} from "../services/utils";

function GoodsItem(props) {
    const {
        id,
        name,
        description,
        price,
        full_background,
        type,
        icon,
        addToBasket = Function.prototype,
    } = props;

    return (
        <div className='card'>
            <div className='card-image'>
                <img src={full_background} alt={name}/>
            </div>
            <div className='card-content'>
                <span className='card-title'>
                    <img className='circle image-icon' src={icon} alt={name} />
                    {name}
                </span>
                <hr/>
                <p>Description: <em>{description}</em></p>
                <p>Type: <em>{type}</em></p>
            </div>
            <div className='card-action'>
                <button
                    className='btn'
                    onClick={() =>
                        addToBasket({
                            id,
                            name,
                            icon,
                            price,
                        })
                    }
                >
                    Add to Cart
                </button>
                <span className='right' style={{fontSize: '1.5rem'}}>
                   ${calcToPrice(price)}
                </span>
            </div>
        </div>
    );
}

export {GoodsItem};