import React from 'react'

function Footer() {
    return <footer className='page-footer blue-grey darken-4'>
        <div className='footer-copyright'>
            <div className='container'>
                © {new Date().getFullYear()} Copyright
                <a
                    className='grey-text text-lighten-4 right'
                    href='https://bitbucket.org/vladgeo/react-showcase-api-e-shop/src/master/'
                >
                    Repo
                </a>
            </div>
        </div>
    </footer>
}

export {Footer}