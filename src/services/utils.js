export function calcToPrice(n, q = 1) {
    return `${(n * q / 100).toFixed(2)} CAD`
}